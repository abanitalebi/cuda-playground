// Color to Greyscale Conversion

//A common way to represent color images is known as RGBA - the color
//is specified by how much Red, Green, and Blue is in it.
//The 'A' stands for Alpha and is used for transparency; it will be
//ignored in this homework.

//Each channel Red, Blue, Green, and Alpha is represented by one byte.
//Since we are using one byte for each color there are 256 different
//possible values for each color.  This means we use 4 bytes per pixel.

//Greyscale images are represented by a single intensity value per pixel
//which is one byte in size.

//The NTSC (National Television System Committee) recommends the following
//formula for color to greyscale conversion:

//I = .299f * R + .587f * G + .114f * B

//Notice the trailing f's on the numbers which indicate that they are 
//single precision floating point constants and not double precision
//constants.

#include "reference_calc.cpp"
#include "utils.h"
#include <stdio.h>

__global__
void rgba_to_greyscale(const uchar4* const rgbaImage,
	unsigned char* const greyImage,
	int numRows, int numCols)
{
	//The output (greyImage) at each pixel should be the result of
	//applying the formula: output = .299f * R + .587f * G + .114f * B;
	//Note: We will be ignoring the alpha channel for this conversion

	int row = blockIdx.x;
	int col = blockIdx.y;

	uchar4 rgba = rgbaImage[row * numCols + col];
	float grey = .299f * rgba.x + .587f * rgba.y + .114f * rgba.z;

	greyImage[row * numCols + col] = grey;

}

void your_rgba_to_greyscale(const uchar4 * const h_rgbaImage, uchar4 * const d_rgbaImage,
	unsigned char* const d_greyImage, size_t numRows, size_t numCols)
{
	const dim3 blockSize(1, 1, 1);
	const dim3 gridSize(numRows, numCols, 1);
	rgba_to_greyscale << <gridSize, blockSize >> >(d_rgbaImage, d_greyImage, numRows, numCols);

	cudaDeviceSynchronize(); 
	checkCudaErrors(cudaGetLastError());
}
